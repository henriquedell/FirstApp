module.exports = function(app) {
    app.get("/noticias", function(request, response) {
        var conn = app.config.db;
        conn.connect(function(error) {
            // if(error) throw error;
            conn.query("SELECT * FROM noticias", function(error,result,fields) {
                response.render("noticias/noticias", {noticias : result});
            });
        });
    });
}