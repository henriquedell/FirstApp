module.exports = function(app) {
    app.get("/", function(request, response) {
        response.render("home/index"); // A Engine EJS já sabe que extensão é .ejs, é a única entendida por ela
    });
}