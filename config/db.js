var mysql = require("mysql");
    
var conn = mysql.createConnection({
    host : "localhost",
    port : "3030", // Necessário por ter alterado a porta do banco aqui no notebook
    user : "root",
    password : "admin",
    database : "portal_noticias"
});

module.exports = conn;